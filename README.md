## HiSS

Contains a repository of HiSS presentations

### Viewing Presentation

To view a presentation, download the repository and open an *.html file in a browser. To view presenter notes, press 'p' in the presentation.